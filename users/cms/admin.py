from django.contrib import admin
from .models import Contenido, Comment


admin.site.register(Contenido)
admin.site.register(Comment)
