from django.db import models
class Contenido(models.Model):
    key = models.CharField(max_length=64)
    value = models.TextField()

    def __str__(self):
        return self.key


class Comment(models.Model):
    content= models.ForeignKey(Contenido, on_delete=models.CASCADE)
    title = models.CharField(max_length=200)
    body = models.TextField(blank=False)
    date = models.DateTimeField('publicado')

    def __str__(self):
        return self.title

